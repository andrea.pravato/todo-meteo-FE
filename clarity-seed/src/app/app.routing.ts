/*
 * Copyright (c) 2016 VMware, Inc. All Rights Reserved.
 * This software is released under MIT license.
 * The full license information can be found in LICENSE in the root directory of this project.
 */
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
import { Routes, RouterModule } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import {TodosComponent} from "./todos/todos.component";
import {NewTodoComponent} from "./new-todo/new-todo-component.component";
import {TodoDetailComponent} from "./todo-detail/todo-detail.component";
import {EditTodoComponent} from "./edit-todo/edit-todo.component";


export const ROUTES: Routes = [
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: 'home', component: HomeComponent},
    {path: 'about', component: AboutComponent},
    {
        path: 'todos',
        children:[
            {
                path: 'new',
                component: NewTodoComponent
            },
            {
                path: ':id',
                children: [
                    {
                        path: '',
                        component: TodoDetailComponent
                    }
                    // ,
                    // {
                    //     path: 'edit',
                    //     component: EditTodoComponent,
                    //     resolve: {
                    //         todo: TodoResolver
                    //     }
                    // }
                ]
            },
            {
                path: '',
                component: TodosComponent
            }
        ]
    },
];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(ROUTES);
