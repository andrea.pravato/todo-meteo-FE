import { Injectable } from '@angular/core';
import {Todo} from "./todo";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/observable/throw';
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import 'rxjs/add/operator/do';

@Injectable()
export class TodoService {

  constructor(private http: HttpClient) { }

    findAllTodos() {
        return this.http.get("http://localhost:9000/todos").map(Todo.fromJsonList);
    }

    createNewTodo(todo:any):Observable<any> {
        //

        let formData: FormData = new FormData();
        for (let element of todo) {
            formData.append(element.name, element);
        }
        let headers = new Headers();
        headers.set('Accept', 'application/json');
        //let options = new RequestOptions({ headers: headers });
        return this.http.post("http://localhost:9000/todos", formData)
            .do(res => console.log("Obtained reply " + JSON.stringify(res)))
            .catch(error => Observable.throw(error));



    }
}
