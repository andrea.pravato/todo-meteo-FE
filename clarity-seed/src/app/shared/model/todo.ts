export class Todo {


    constructor(
        public id:string,
        public title:string,
        public locationAddress:string,
        public timestamp: number,
        public locationLat: number,
        public locationLong: number,
        public weatherForecast: string){

    }


    static fromJsonList(array): Todo[] {
        return array.map(Todo.fromJson);
    }

    static fromJson({id, title, locationAddress,
                        timestamp,locationLat,locationLong,weatherForecast}):Todo {
        return new Todo(
            id,
            title,
            locationAddress,
            timestamp,
            locationLat,
            locationLong,
            weatherForecast);
    }


}
