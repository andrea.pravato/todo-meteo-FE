import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Todo} from "../shared/model/todo";
import {TodoService} from "../shared/model/todo.service";


@Component({
  selector: 'app-todos',
  providers: [TodoService],
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {
    todos$: Observable<Todo[]>;

    constructor(private todoService: TodoService) { }

    ngOnInit() {
        this.todos$ = this.todoService.findAllTodos();
    }

}
