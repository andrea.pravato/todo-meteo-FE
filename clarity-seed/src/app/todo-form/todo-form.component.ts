import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.scss']
})
export class TodoFormComponent implements OnInit, OnChanges {

    initialValue: any;

    form: FormGroup;

    constructor(private fb: FormBuilder) {
        this.form = this.fb.group({
            title: ['',Validators.required],
            timestamp: [''],
            locationAddress: [''],
            locationLat: [''],
            locationLong: [''],
            weatherForecast: ['']
        });
    }

    ngOnInit() {
    }

    ngOnChanges(changes:SimpleChanges){
        if(changes['initialValue']){
            this.form.patchValue(changes['initialValue'].currentValue)
        }
    }

    isErrorVisible(field: string, error: string){
        return this.form.controls[field].dirty
            && this.form.controls[field].errors &&
            this.form.controls[field].errors[error];
    }

    reset(){
        this.form.reset();
    }

    get valid(){
        return this.form.valid;
    }

    get value(){
        return this.form.value;
    }

}
