import { Component, OnInit } from '@angular/core';
import {TodoService} from "../shared/model/todo.service";

@Component({
  selector: 'app-new-todo-component',
  providers: [TodoService],
  templateUrl: './new-todo-component.component.html',
  styleUrls: ['./new-todo-component.component.scss']
})
export class NewTodoComponent implements OnInit {

  constructor(private todoService: TodoService) { }

  ngOnInit() {

  }

    save(form){

      this.todoService.createNewTodo(form.value)
        .subscribe(
            ()=>{
                alert("todo created successfully. Create another todo?");
                form.reset();
            },
            err => alert(`error creating todo` + JSON.stringify(err.error))
        );

    }

}
